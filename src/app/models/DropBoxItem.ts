import { TreeNode } from 'primeng/api';
export interface DropBoxItem extends TreeNode{
    '.tag': string;
    client_modified:string;
    content_hash:string;
    id:string;
    is_downloadable:boolean;
    name:string;
    path_display:string;
    path_lower:string;
    rev:string;
    server_modified:string;
    size:string;
}