import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { AppComponent } from "./app.component";
import { NoopAnimationsModule }  from '@angular/platform-browser/animations';
import { FileListComponent } from './file-list/file-list.component';
import { TreeModule } from 'primeng/tree';
import { RouterModule, Routes } from '@angular/router'
const routes: Routes = []
@NgModule({
  declarations: [AppComponent, FileListComponent],
  imports: [
    BrowserModule,  
    RouterModule.forRoot(routes),
    NoopAnimationsModule,
    TreeModule 
   
    ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
