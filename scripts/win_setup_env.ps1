cd ..
echo "Install rust"
curl -o rustup-init.exe https://static.rust-lang.org/rustup/dist/x86_64-pc-windows-msvc/rustup-init.exe
./rustup-init.exe -y

# echo "Remove installed node from path and install new node"
# $path = $env:Path
# $newpath = $path.replace("C:\Program Files\nodejs\;","")
# $env:Path = $newpath

# $env:Path

# curl -o node.zip https://nodejs.org/dist/v16.16.0/node-v16.16.0-win-x64.zip
# Expand-Archive ./node.zip ./
# $env:Path += ";$pwd\node-v16.16.0-win-x64\;"

# $env:Path

echo "Install choco"
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))

$env:ChocolateyInstall = Convert-Path "$((Get-Command choco).Path)\..\.."   
Import-Module "$env:ChocolateyInstall\helpers\chocolateyProfile.psm1"

refreshenv

# node -v

# npm i -g yarn

# npm -v

# echo "Install pnpm"
# npm i -g pnpm

refreshenv

# pnpm config set store-dir .pnpm-store
# pnpm setup

exit
